import webbrowser
from flask import Flask, render_template
import serial
import time
import sys

# JM: import site directory from wherever it is on the user's machine
# SITE is defined in __init__.py
app = Flask(__name__, template_folder='./site', static_folder='./site')

arduino = serial.Serial("COM3",115200)

LOG = ""


@app.route('/')
def main():
    return update()


# X STAGE
@app.route('/right', methods=['GET', 'POST'])
def right():
    arduino.write(b"M RX1 300")
    return update()

@app.route('/left', methods=['GET', 'POST'])
def left():
    arduino.write(b"M RX1 -300")
    return update()


# Y STAGE
@app.route('/forward', methods=['GET', 'POST'])
def forward():
    arduino.write(b"M RY1 300")
    return update()

@app.route('/backward', methods=['GET', 'POST'])
def backward():
    arduino.write(b"M RY1 -300")
    return update()


# Z STAGE
@app.route('/up', methods=['GET', 'POST'])
def up():
    arduino.write(b"M RZ1 +100")
    return update()

@app.route('/down', methods=['GET', 'POST'])
def down():
    arduino.write(b"M RZ1 -100")
    return update()


# OTHER
@app.route('/update', methods=['GET', 'POST'])
def update():
    console()
    return render_template('controller.html', arduino_log=LOG)

def console():
    global LOG
    LOG += arduino.read( arduino.in_waiting ).decode('utf-8')
    return LOG




# import pdb; pdb.set_trace()

if (__name__ == "__main__"):
    # scheduler = APScheduler()
    # scheduler.add_job(func=update, trigger='interval', id='job', seconds=3)
    # scheduler.start()
    webbrowser.open("http://localhost:8000")
    app.run(port = 8000)
