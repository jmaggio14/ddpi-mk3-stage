/*
   Jeff Maggio
   Active Perception Lab
   Aug 23rd, 2019

   Version 1.1
*/



// SUPER COMMANDS
#define MOVEMENT_COMMAND 'M'
#define QUERY_COMMAND '?'
#define DISABLE_LIMITS_COMMAND 'X'
#define ENABLE_LIMITS_COMMAND 'E'

#define DEFAULT_PULSE_WIDTH 500 //microseconds
#define DIRECTION_FORWARD 1
#define DIRECTION_BACKWARD 0

#define ENABLE_SIGNAL 0
#define BUTTON_DIRECTION_PIN A7
#define BUTTON_ACTIVE_SIGNAL 0
#define DIRECTION_ACTIVE_SIGNAL 0

#define READY_LED_PIN A1
#define LIMIT_WARNING_LED_PIN A0

////////////////////////////// SETTING UP GLOBALS /////////////////////////////////
enum ERRORS {
  NO_ERROR,
  INVALID_COMMAND,
  INVALID_MOTOR_ID,
  BUTTON_MODE,

  //L
  LX1_LIM_NEGATIVE,
  LX1_LIM_POSITIVE,

  LY1_LIM_NEGATIVE,
  LY1_LIM_POSITIVE,

  LY2_LIM_NEGATIVE,
  LY2_LIM_POSITIVE,

  LZ1_LIM_NEGATIVE,
  LZ1_LIM_POSITIVE,

  // R
  RX1_LIM_NEGATIVE,
  RX1_LIM_POSITIVE,

  RY1_LIM_NEGATIVE,
  RY1_LIM_POSITIVE,

  RY2_LIM_NEGATIVE,
  RY2_LIM_POSITIVE,

  RZ1_LIM_NEGATIVE,
  RZ1_LIM_POSITIVE,
};

const char* ERROR_MSG[] {
  "ready",
  "ERROR: unrecognized command received - type '?' to see valid commands",
  "ERROR: unrecognized motor ID. must be one of: ",
  "Button mode enabled",
  //L
  "ERROR: NEGATIVE LX1 limit reached!",
  "ERROR: POSITIVE LX1 limit reached!",

  "ERROR: NEGATIVE LY1 limit reached!",
  "ERROR: POSITIVE LY1 limit reached!",

  "ERROR: NEGATIVE LY2 limit reached!",
  "ERROR: POSITIVE LY2 limit reached!",

  "ERROR: NEGATIVE LZ1 limit reached!",
  "ERROR: POSITIVE LZ1 limit reached!",

  // R
  "ERROR: NEGATIVE RX1 limit reached!",
  "ERROR: POSITIVE RX1 limit reached!",

  "ERROR: NEGATIVE RY1 limit reached!",
  "ERROR: POSITIVE RY1 limit reached!",

  "ERROR: NEGATIVE RY2 limit reached!",
  "ERROR: POSITIVE RY2 limit reached!",

  "ERROR: NEGATIVE RZ1 limit reached!",
  "ERROR: POSITIVE RZ1 limit reached!",
};


// define a structure to represent all pins and attributes of a motor
typedef struct {
  // attributes
  char* motor_id; //0
  int direction_pin; //1
  int pulse_pin; //2

  int positive_switch_pin; //3
  int negative_switch_pin; //4

  int button_pin; //5
  int enable_pin; //6

  int pulse_duration; //7
  int cur_direction; //8
  long step_count; //9

  int pos_limit_error;
  int neg_limit_error;

} Motor;

// array of all motor objects
const Motor MOTORS[] {
  //id, dir, pulse, pos_switch, neg_switch, button_pin, enable_pin, pulse_dur, cur_dir, step count, pos_limit_error, neg_limit_error
  {"RX1", 46, 47, 31, 10, A8,  9,  DEFAULT_PULSE_WIDTH, DIRECTION_FORWARD, 0, RX1_LIM_POSITIVE, RX1_LIM_NEGATIVE},
  {"RY1", 48, 49, 33, 32, A9,  8,  DEFAULT_PULSE_WIDTH, DIRECTION_FORWARD, 0, RY1_LIM_POSITIVE, RY1_LIM_NEGATIVE},
  {"RY2", 50, 51, 35, 34, A10, 7,  DEFAULT_PULSE_WIDTH, DIRECTION_FORWARD, 0, RY2_LIM_POSITIVE, RY2_LIM_NEGATIVE},
  {"RZ1", 52, 53, 37, 36, A11, 6,  DEFAULT_PULSE_WIDTH, DIRECTION_FORWARD, 0, RZ1_LIM_POSITIVE, RZ1_LIM_NEGATIVE},
  
  {"LX1", 38, 39, 23, 22, A12, 13, DEFAULT_PULSE_WIDTH, DIRECTION_FORWARD, 0, LX1_LIM_POSITIVE, LX1_LIM_NEGATIVE},
  {"LY1", 40, 41, 25, 24, A13, 12, DEFAULT_PULSE_WIDTH, DIRECTION_FORWARD, 0, LY1_LIM_POSITIVE, LY1_LIM_NEGATIVE},
  {"LY2", 42, 43, 27, 26, A14, 11, DEFAULT_PULSE_WIDTH, DIRECTION_FORWARD, 0, LY2_LIM_POSITIVE, LY2_LIM_NEGATIVE},
  {"LZ1", 44, 45, 29, 28, A15, 10, DEFAULT_PULSE_WIDTH, DIRECTION_FORWARD, 0, LZ1_LIM_POSITIVE, LZ1_LIM_NEGATIVE},
};


// whether or not to enable limtis
// JM: disabled for testing purposes - Aug 6, 2019
bool ENABLE_LIMITS = true;

//Number of motors we support right now
int NUM_MOTORS = sizeof(MOTORS) / sizeof(Motor);


//////////////////////////////// HELPERS //////////////////////////////
void error(int code) {
  Serial.print( ERROR_MSG[code] );

  // Special error cases
  if (code == INVALID_MOTOR_ID) {
    for (int i = 0; i < NUM_MOTORS; i++) {
      Serial.print(MOTORS[i].motor_id);
      Serial.print(", ");
    }
  }


  Serial.println("\nready");
}


// LEDS
void turnOnReadyLED(){
  digitalWrite(READY_LED_PIN, HIGH);
}

void turnOffReadyLED(){
  digitalWrite(READY_LED_PIN, LOW);
}


void turnOnLimitLED(){
  digitalWrite(LIMIT_WARNING_LED_PIN, HIGH);
}

void turnOffLimitLED(){
  digitalWrite(LIMIT_WARNING_LED_PIN, LOW);
}


//////////////////////////////////// MOTOR DRIVING FUNCTIONS ////////////////////////////////////
inline void enableMotor(Motor* motor) {
  digitalWrite(motor->enable_pin, ENABLE_SIGNAL);
}


inline void disableMotor(Motor* motor) {
  digitalWrite(motor->enable_pin, !(ENABLE_SIGNAL) );
}


inline void disableAll() {
  for (int i = 0; i < NUM_MOTORS; i++) {
    disableMotor(&MOTORS[i]);
  }
}


inline void enableAll() {
  for (int i = 0; i < NUM_MOTORS; i++) {
    enableMotor(&MOTORS[i]);
  }
}


inline void setDirection(Motor* motor, bool direction_) {
  motor->cur_direction = direction_;
  digitalWrite(motor->direction_pin, motor->cur_direction);
}


inline int checkLimit(Motor* motor) {
  // return zero if limits are disabled
  if ( !(ENABLE_LIMITS) ) {
    turnOffLimitLED();
    return 0;
  }

  // check if the positive switch is triggered
  if (digitalRead(motor->positive_switch_pin) && motor->cur_direction) {
    turnOnLimitLED();
    error(motor->pos_limit_error);
    return 1;
  }

  // check if the negative switch is triggered
  if (digitalRead(motor->negative_switch_pin) && !(motor->cur_direction)) {
    turnOnLimitLED();
    error(motor->neg_limit_error);
    return -1;
  }
  turnOffLimitLED();
  return 0;
}

inline void stepMotor(Motor* motor) {
  if ( checkLimit(motor) != 0 ) {
    return;
  }
  // write a pulse to the pin
  digitalWrite(motor->pulse_pin, HIGH);
  delayMicroseconds(motor->pulse_duration);
  digitalWrite(motor->pulse_pin, LOW);
  delayMicroseconds(motor->pulse_duration);

}

//// MOTOR Control - Buttons
inline int buttonDrive() {
  bool button_pressed = 0;
  for (int i = 0; i < NUM_MOTORS; i++) {
    //step the motor forward if its button is pressed
    if ( digitalRead(MOTORS[i].button_pin) == BUTTON_ACTIVE_SIGNAL ) {
      enableMotor(&MOTORS[i]);
      setDirection(&MOTORS[i], digitalRead(BUTTON_DIRECTION_PIN) );
      stepMotor(&MOTORS[i]);
      button_pressed = true;
    }
    else {
      disableMotor(&MOTORS[i]);
    }

  }
  return button_pressed;
}


//// MOTOR Control - Serial Port
void serialDrive(String data) {
  // turn off the ready LED
  turnOffReadyLED();
  
  String motor_id = data.substring(0, 3);
  long num_pulses = data.substring(3).toInt();
  Motor motor;
  int error_code = INVALID_MOTOR_ID;

  Serial.print("moving ");
  Serial.print(motor_id);
  Serial.print(" ");
  Serial.print(num_pulses);
  Serial.println(" steps");

  // search through all of our pins and check to see if the motor
  // is known. if it is, then fetch the pin number
  for (int i = 0; i < NUM_MOTORS; i++) {
    if (strcmp(MOTORS[i].motor_id, motor_id.c_str()) == 0) {
      motor = MOTORS[i];
      error_code = NO_ERROR;
      break;
    }
  }
  // end the function if we can't find the specified motor
  if (error_code != NO_ERROR) {
    error(error_code);
    return;
  }

  // Enable the motor
  enableMotor(&motor);

  // check the number of pulses and modify the motor direction pin
  if (num_pulses < 0) {
    setDirection(&motor, DIRECTION_BACKWARD);
  }
  else {
    setDirection(&motor, DIRECTION_FORWARD);
  }

  // step the motor for every pulse
  for (int i = 0; i < abs(num_pulses); i++) {
    stepMotor(&motor);
  }

  // increment the motor's step counter
  motor.step_count = motor.step_count + num_pulses;


  // Disable the motor
  disableMotor(&motor);


  error(error_code);


  // turn on the ready LED
  turnOnReadyLED();
} // end serialDrive


//////////////////////////////////// QUERY FUNCTIONS ////////////////////////////////////

void query() {
  ////// Motor Stats ////
  // for each motor
  for (int i = 0; i < NUM_MOTORS; i++) {
    Motor motor = MOTORS[i];
    // motor id
    Serial.println(motor.motor_id);
    //pulse pin
    Serial.print("\tpulse pin: ");
    Serial.println(motor.pulse_pin, DEC);
    //direction pin
    Serial.print("\tdirection pin: ");
    Serial.println(motor.direction_pin, DEC);
    //positive switch pin
    Serial.print("\tpositive switch pin: ");
    Serial.println(motor.positive_switch_pin, DEC);
    //negative switch pin
    Serial.print("\tnegative swtich pin: ");
    Serial.println(motor.negative_switch_pin, DEC);
    //button pin
    Serial.print("\tbutton pin: ");
    Serial.println(motor.button_pin, DEC);
    //enable pin
    Serial.print("\tenable pin: ");
    Serial.println(motor.enable_pin, DEC);
    //pulse duration
    Serial.print("\tpulse duration: ");
    Serial.print(motor.pulse_duration, DEC);
    Serial.println("us");
    //current direction
    Serial.print("\tcurrent direction: ");
    if (motor.cur_direction == DIRECTION_FORWARD) {
      Serial.print(motor.cur_direction, DEC);
      Serial.println(" | forward");
    }
    else {
      Serial.print(motor.cur_direction, DEC);
      Serial.println(" | backward");
    }
    //step counter
    Serial.print("\tstep count : ");
    Serial.println(motor.step_count, DEC);
  }


  ///// how to use commands //////
  Serial.println("COMMANDS:");
  //movement
  Serial.println("\tMOVEMENT:  'M <motor_id> <num_steps>' - e.g. 'M LX1 1000'");
  //Query
  Serial.print("\tQUERY:  ");
  Serial.println(QUERY_COMMAND);
  //Disable Limits
  Serial.print("\tDISABLE LIMITS:  ");
  Serial.println(DISABLE_LIMITS_COMMAND);
  //Enable Limits
  Serial.print("\tENABLE LIMITS:  ");
  Serial.println(ENABLE_LIMITS_COMMAND);

  ////// Global variables /////
  //limits
  Serial.print("limits enabled: ");
  Serial.println( ENABLE_LIMITS );
  //buttons info
  Serial.print("button direction pin: ");
  Serial.println(BUTTON_DIRECTION_PIN);

  error(NO_ERROR);
}




//////////////////////////////////// MAIN FUNCTIONS ////////////////////////////////////
void setup() {
  Serial.begin(115200);

  // initialize all the pins defined in our motor dictionary
  for (int i = 0; i < NUM_MOTORS; i++) {
    Serial.print("initializing motor: ");
    Serial.print(String(MOTORS[i].motor_id));
    Serial.println("...");

    //direction
    pinMode(MOTORS[i].direction_pin, OUTPUT);
    digitalWrite(MOTORS[i].direction_pin, LOW);

    //pulse pin
    pinMode(MOTORS[i].pulse_pin, OUTPUT);
    digitalWrite(MOTORS[i].pulse_pin, LOW);

    //switches
    pinMode(MOTORS[i].positive_switch_pin, INPUT_PULLUP);
    pinMode(MOTORS[i].negative_switch_pin, INPUT_PULLUP);

    //button pin
    pinMode(MOTORS[i].button_pin, INPUT_PULLUP);

    //enable pin
    pinMode(MOTORS[i].enable_pin, OUTPUT);
    digitalWrite(MOTORS[i].enable_pin, !(ENABLE_SIGNAL) );

  }

  // Other Pins
  pinMode(BUTTON_DIRECTION_PIN, INPUT_PULLUP);
  pinMode(READY_LED_PIN, OUTPUT);
  pinMode(LIMIT_WARNING_LED_PIN, OUTPUT);
  
  turnOnReadyLED();
  turnOffLimitLED();
  error(NO_ERROR);
}


void loop() {
  // put your main code here, to run repeatedly:

  /* Check the Buttons first to see if they are already pressed

      This will take priority over serial commands. The serial buffer will
      be flushed if any buttons have been pressed
  */
  if ( buttonDrive() ) {
    // a button has been pressed, so we have to flush the serial bus.
    Serial.flush();
    while (Serial.available()) Serial.read();
    return;
  }


  /*
     Read Serial Bus and parse the input
  */
  String msg = Serial.readStringUntil('\n');
  msg.replace(" ", ""); //remove all whitespace
  msg.toUpperCase(); // make everything uppercase

  // return if no message was received
  if (msg == "") {
    return;
  }

  char command_id = msg.charAt(0);
  String data = msg.substring(1);

  // switch depending on what command char was recieved
  switch (command_id) {
    // move the motor
    case MOVEMENT_COMMAND:
      serialDrive(data);
      break;
    // print out statistics
    case QUERY_COMMAND:
      query();
      break;
    // disable the limit switches
    case DISABLE_LIMITS_COMMAND:
      ENABLE_LIMITS = false;
      Serial.println("limits disabled");
      break;
    // enable the limit switches
    case ENABLE_LIMITS_COMMAND:
      ENABLE_LIMITS = true;
      Serial.println("limits enabled");
      break;
    default:
      error(INVALID_COMMAND);
      break;

  }
}
